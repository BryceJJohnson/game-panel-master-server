const WebUtils 	= require('../scripts/utils/Web');
const Users 	= require('../scripts/services/Users');
const Logger 	= require('../scripts/utils/Logger');

module.exports = function(app) {
	app.options('/users/login', function(req, res) {
		Logger.silly('[WEB | Users | Login | OPTIONS]');
		
		res.header("access-control-allow-methods", "POST, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.post('/users/login', function(req, res){
		Logger.debug('[WEB | Users | Login | POST] EMail (' + req.body.email + ' & ****)');
		
		Users.loginUser(req.body.email, req.body.pass).then(function(user) {
			if(!user) {
				res.json(WebUtils.response(true, 'User does not exist'));
				return
			}
			
			Logger.debug('[WEB | Users | Login] ' + req.body.email + ' Success!');
			res.json(WebUtils.response(false, 'Logged in', JSON.stringify(user)));
		}, function(err) {
			Logger.error('[WEB | Users | Login] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
	
	app.options('/users/info/:UserID', function(req, res) {
		Logger.silly('[WEB | Users | Info | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/users/info/:UserID', WebUtils.checkToken, function(req, res) {
		Logger.debug('[WEB | Users | Info | GET] ID (' + req.params.UserID + ')');
		
		if(req.params.UserID != req.UserID) {
			res.json(WebUtils.response(true, 'Access Denied'));
			return;
		}
		
		Users.getUserById(req.params.UserID).then(function(user) {
			if(!user) {
				res.json(WebUtils.response(true, 'User does not exist'));
				return
			}

			res.json(WebUtils.response(false, 'Success', JSON.stringify(user)));
		}, function(err) {
			Logger.error('[WEB | Users | Info | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	});
}