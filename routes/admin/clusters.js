const WebUtils 	= require('../../scripts/utils/Web');
const Logger 	= require('../../scripts/utils/Logger');

const Clusters 	= require('../../scripts/services/Clusters');

module.exports = function(app) {
	
	app.options('/admin/clusters', function(req, res) {
		Logger.silly('[WEB | Admin | Clusters | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/admin/clusters', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | Admin | Clusters | GET]');
		
		Clusters.getAll().then(function(result) {
			if(!result) {
				res.json(WebUtils.response(true, 'Error getting clusters'));
				return
			}
			
			res.json(WebUtils.response(false, 'Success', JSON.stringify(result)));
		}, function(err) {
			Logger.error('[WEB | Admin | Clusters | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
}