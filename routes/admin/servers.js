const WebUtils 	= require('../../scripts/utils/Web');
const Logger 	= require('../../scripts/utils/Logger');

const Servers 		= require('../../scripts/services/Servers');
const Communication = require('../../scripts/comm/server');

module.exports = function(app) {
	
	app.options('/admin/servers', function(req, res) {
		Logger.silly('[WEB | Admin | servers | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/admin/servers', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | Admin | servers | GET]');
		
		Servers.getAll().then(function(result) {
			if(!result) {
				res.json(WebUtils.response(true, 'Error getting servers'));
				return
			}
			
			res.json(WebUtils.response(false, 'Success', JSON.stringify(result)));
		}, function(err) {
			Logger.error('[WEB | Admin | servers | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
	
	app.options('/admin/servers/:ServerID', function(req, res) {
		Logger.silly('[WEB | Admin | servers/:ServerID | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/admin/servers/:ServerID', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | Admin | servers/:ServerID | GET]');
		
		var remoteServer = Communication.getRemoteServerById(req.params.ServerID);
		if(remoteServer) {
			remoteServer.getServerStatus().then(function(result) {
				if(!result) {
					res.json(WebUtils.response(true, 'Error running command'));
					return;
				}
					
				res.json(WebUtils.response(false, 'Got result', JSON.stringify(result)));
			}, function(err) {
				Logger.error('[WEB | Admin | servers/:ServiceID | GET] Internal Error');
				res.json(WebUtils.response(true, 'Internal error...'));
			});
		} else {
			res.json(WebUtils.response(true, 'Remote server offline'));
		}
	})
}