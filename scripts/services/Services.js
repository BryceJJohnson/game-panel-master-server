const Config 	= require('../utils/Config');

const Logger 	= require('../utils/Logger');
const MySQL 	= require('../utils/MySQL');

const Q 		= require('q');

module.exports = {
	getAllAdmin: function() {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM services;', [], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	getAll: function(userId) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM services WHERE OwnerID=?;', [userId], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	getAllByServer: function(userId, serverId) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM services WHERE OwnerID=? AND ServerID=?;', [userId,serverId], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	get: function(serviceId) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM services WHERE ServiceID=?;', [serviceId], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					
					if(!rows || rows.length != 1) {
						deferred.reject();
						return;
					}
					
					deferred.resolve(rows[0]);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	}
};