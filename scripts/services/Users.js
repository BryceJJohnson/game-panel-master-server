const Config 	= require('../utils/Config');

const Logger 	= require('../utils/Logger');
const MySQL 	= require('../utils/MySQL');

const Q 		= require('q');
const JWT 		= require('jsonwebtoken');

module.exports = {
	getUserByEmail: function(email) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM users WHERE EMail=? LIMIT 1;', [email], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	getUsers: function() {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT UserID,BillingID,GroupID,FirstName,LastName,EMail,LastLogin,RegisterDate FROM users;', [], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	getUserById: function(userId) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT UserID,BillingID,GroupID,FirstName,LastName,EMail,LastLogin,RegisterDate FROM users WHERE UserID=? LIMIT 1;', [userId], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					
					if(!rows || rows.length != 1) {
						deferred.reject();
						return;
					}
					
					deferred.resolve(rows[0]);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	getUserByToken: function(token) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT UserID FROM users WHERE Token=? LIMIT 1;', [token], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	updateToken: function(userId, token) {
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
				return;    
			});
			
			connection.query('UPDATE users SET Token=? WHERE UserID=?;', [token, userId], function(err, rows, fields) {
				connection.release();
			});
		})
	},
	
	ftpLogin: function(email, pass) {
		var deferred = Q.defer();
		
		module.exports.getUserByEmail(email).then(function(result) {
			if(!result || result.length != 1) {
				deferred.reject();
				return;
			}

			deferred.resolve(result[0].UserID);
		}, function(err) {
			deferred.reject(err);
			return;  
		})
		
		return deferred.promise;
	},
	
	loginUser: function(email, password) {
		var deferred = Q.defer();
		
		module.exports.getUserByEmail(email).then(function(result) {
			if(!result || result.length != 1) {
				deferred.reject();
				return;
			}
			
			const user = result[0];
			
			var token = JWT.sign({
				id: result[0].UserID
			}, Config.token_secret, { expiresIn: '1h' });
			
			module.exports.updateToken(user.UserID, token);

			deferred.resolve(token);
		}, function(err) {
			deferred.reject(err);
			return;  
		})
		
		return deferred.promise;
	}
};