const Config 	= require('../utils/Config');

const Logger 	= require('../utils/Logger');
const MySQL 	= require('../utils/MySQL');

const Q 		= require('q');

module.exports = {
	getAll: function() {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM servers;', [], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	getByCluster: function(clusterId) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM servers WHERE ClusterID=?;', [clusterId], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	getBySecret: function(key) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM servers WHERE SecretKey=? LIMIT 1;', [key], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					if(!rows || rows.length != 1) {
						deferred.reject();
						return;
					}
					
					deferred.resolve(rows[0]);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	get: function(serverId) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM servers WHERE ServerID=?;', [serverId], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					
					if(!rows || rows.length != 1) {
						deferred.reject();
						return;
					}
					
					deferred.resolve(rows[0]);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	}
};