const winston 	= require('winston');
const fs 		= require('fs');

const tsFormat 	=  function() { return Math.floor(Date.now()/1000)+'' };

const logDir 	= 'logs';
if (!fs.existsSync(logDir)) {
	fs.mkdirSync(logDir);
}

var logger 	= new (winston.Logger)( {
	level: "verbose",
	handleExceptions: false,
	transports: [
		// colorize the output to the console
		new (winston.transports.Console)({
			timestamp: tsFormat,
			colorize: true
		}),
		new (winston.transports.File)({
			filename: logDir+'/results.log',
			timestamp: tsFormat
		})
	],
	exceptionHandlers: [
		new (winston.transports.Console)({
			timestamp: tsFormat,
			colorize: true
		}),
		new winston.transports.File({ 
			filename: logDir+'/results.log',
			timestamp: tsFormat
		})
    ]
});

module.exports = logger;