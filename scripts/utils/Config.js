const fs 		= require('fs');
const Logger 	= require('./Logger');

var configData;

try {
	var configFile 	= fs.readFileSync('./config.json');
	configData = JSON.parse(configFile);
	
	if(configData.host_port === undefined || configData.ssl_port === undefined ||
	configData.log_level === undefined || configData.ssl_mode === undefined || 
	configData.ssl_cert === undefined || configData.ssl_key === undefined ||
	configData.db_host === undefined || configData.db_user === undefined || 
	configData.db_pass === undefined || configData.db_name === undefined || 
	configData.db_port === undefined || configData.token_secret === undefined) {
		Logger.error("[GP | Config] Config File Missing Data - EXITING", {}, function(err, level, msg, meta) {
			Logger.transports.file.on('flush', function() {
				process.exit(1);
			});
		});
	}
	
	Logger.level = configData.log_level;
	
  } catch (err) {
    Logger.error("[GP | Config] Config File Read Error - EXITING", {}, function(err, level, msg, meta) {
		Logger.transports.file.on('flush', function() {
			process.exit(1);
		});
	});
}

module.exports = configData;