const Config 	= require('./Config.js');
const Logger 	= require('./Logger');

const Q			= require('q');
const mysql 	= require('mysql');

var pool = mysql.createPool({
	connectionLimit : 100,
	host	 : Config.db_host,
	user     : Config.db_user,
	password : Config.db_pass,
	database : Config.db_name,
	port	 : Config.db_port,
	debug    :  false
});

module.exports = {
	getConnection: function() {
		var deferred = Q.defer();

		pool.getConnection(function(err, connection){
			if (err) {
			  deferred.reject("Error in connection to DB");
			  return;
			}  

			Logger.silly('[MySQL] GET CONNECTION FROM POOL (ID=' + connection.threadId + ')');
			
			deferred.resolve(connection);
		});
		
		return deferred.promise;
	}
};