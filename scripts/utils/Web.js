const Config 	= require('./Config');

const Logger 	= require('./Logger');
const Users 	= require('../services/Users');

const JWT 		= require('jsonwebtoken');

module.exports = {
	response: function(error, message, data) {
		data 	= typeof data  !== 'undefined' ? data : {};
		message = typeof message  !== 'undefined' ? message : '';
		
		var response = {};
		response.result 	= true;
		
		if(error) {
			response.result = false;
		}
		
		response.message 	= message;
		response.data		= data;
		
		return response;
	},
	
	checkAdmin: function(userId) {
		
	},
	
	checkToken: function(req, res, next) {
		var token 	= req.get('Authorization').split('"').join('');
		
		if(token) {
			JWT.verify(token, Config.token_secret, function(err, decoded) {
				if (err) {
					res.json(module.exports.response(true, 'Token Validation Error'));
					return;
				}
				
				Users.getUserByToken(token).then(function(result) {
					if(!result || result.length != 1) {
						res.json(module.exports.response(true, 'No Such API Token'));
						return;
					}
					
					var decoded = JWT.decode(token);
					
					if(result[0].UserID == decoded.id) {
						req.UserID = result[0].UserID;
						next();
					} else {
						res.json(module.exports.response(true, 'Token Not Valid For UserID'));
					}
				}, function(err) {
					res.json(module.exports.response(true, 'Token Query Error'));
				});
			});
		} else {
			res.json(module.exports.response(true, 'No API Token Given'));
		}
	}
}