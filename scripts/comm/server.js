var fs 		= require('fs');
var path 	= require('path');

var clients = require( 'secret' );
var servers = require( 'secret' );

var Logger 			= require('../utils/Logger');
var ServerService 	= require('../services/Servers');

var io;

module.exports = {
	setupComm: function(httpServer) {
		io 	= require('socket.io')(httpServer);
		
		//io.set('transports', ['websocket']);
		io.set('heartbeat timeout', 20000); 
		io.set('heartbeat interval', 2000);

		// Add a connect listener
		io.sockets.on('connection', function(socket) { 

			Logger.debug('[COMM] Client connected');
			
			socket.remoteServer = false;
			
			socket.emit('auth/remote/request');
			Logger.debug('[COMM] auth/remote/request');
			
			socket.on('auth/remote/response', function(data) {
				
				ServerService.getBySecret(data.key).then(function(result) {
					if(result) {
						Logger.debug('[COMM] Remote Server Authenticated');
						socket.emit('auth/remote/accepted');
						
						require('./commands/util')(socket);
						require('./commands/control')(socket);
						
						socket.remoteServer = true;
						socket.ServerID 	= result.ServerID;
						
						servers.set( result.ServerID, socket );
					} else {
						Logger.debug('[COMM] Remote Server REJECTED');
						socket.emit('auth/remote/rejected');
					}
				}, function(error) {
					Logger.error('[COMM] Remote Server REJECTED ' + error);
					socket.emit('auth/remote/rejected');
				});
				
			});

			// Disconnect listener
			socket.on('disconnect', function(reason) {
				if(socket.remoteServer == true) {
					servers.remove(socket.ServerID);
					Logger.debug('[COMM] Remote disconnected ' + reason);
				} else {
					clients.remove(socket);
					Logger.debug('[COMM] Client disconnected ' + reason);
				}
			});
		});
	},
	
	getRemoteServerById: function(Id) {
		return servers.get(Id);
	}
}