const Config 	= require('../../utils/Config');
const Services 	= require('../../services/Services');
const Templates = require('../../services/Templates');
const Users 	= require('../../services/Users');
const Logger 	= require('../../utils/Logger');

const Q 		= require('q');
const async 	= require('async');
const JWT 		= require('jsonwebtoken');

module.exports = function(socket) {
	
	socket.getServiceStatus = function(serviceID) {
		var deferred = Q.defer();
		
		Services.get(serviceID).then(function(service) {
			
			if(!service) {
				deferred.reject('Error retriving service');
				return;
			}
			
			socket.emit('remote/service/status', {service:service}, function(result) {
				if(result) {
					deferred.resolve(result);
				} else {
					Logger.error('[COMM | UTIL | getServiceStatus - Services.Get] Internal Error (SOCKET)');
					deferred.reject('Internal error...');
				}
			})
			
		}, function(err) {
			Logger.error('[COMM | UTIL | getServiceStatus - Services.Get] Internal Error');
			deferred.reject('Internal error...');
		})
		
		
		return deferred.promise;
	},
	
	socket.getServerStatus = function() {
		var deferred = Q.defer();
		
		socket.emit('remote/stats', {}, function(result) {
			if(result) {
				deferred.resolve(result);
			} else {
				Logger.error('[COMM | UTIL | getServerStatus - Services.Get] Internal Error (SOCKET)');
				deferred.reject('Internal error...');
			}
		})
		
		return deferred.promise;
	}
	
	socket.on('auth/client/token', function(data, callback) {
		JWT.verify(token, Config.token_secret, function(err, decoded) {
			if (err) {
				callback(null);
				return;
			}
			
			var decoded = JWT.decode(token);
			callback(decoded);
		});
	})
	
	socket.on('util/ftpLogin', function(data, callback) {
		Logger.debug('[COMM | UTIL | ftpLogin] ' + data.email);
		Users.ftpLogin(data.email, data.pass).then(function(result) {
			callback(result)
		}, function(error) {
			callback(false)
		})
	})
	
	socket.on('util/getLocalServices', function(data, callback) {
		Logger.debug('[COMM | UTIL | getLocalServices] ' + data.user + " " + data.server);
		
		var response 		= {};
		response.services 	= [];
		response.templates 	= {};
		
		Services.getAllByServer(data.user, data.server).then(function(results) {

			response.services 	= results;
	
			async.forEachOf(response.services, function (service, index, cb) {
				
				if(!response.templates[service.TemplateID]) {
					Templates.get(service.TemplateID).then(function(result) {
						response.templates[result.TemplateID] = result;
						cb();
					}, function(error) {
						console.log(error);
						response.templates[result.TemplateID] = null;
						cb(true);
					});
				} else {
					cb();
				}
			}, function(err) {
				if(err) {
					console.log(err);
					callback(false);
					return;
				}
				console.log(response);
				callback(response);
			});
			
		}, function(error) {
			console.log(err);
			callback(false)
		})
	})
}