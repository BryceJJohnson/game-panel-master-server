const Q 		= require('q');
const Services 	= require('../../services/Services');
const Templates = require('../../services/Templates');

module.exports = function(socket) {
	
	socket.reinstallService = function(serviceID) {
		var deferred = Q.defer();
		
		Services.get(serviceID).then(function(service) {
			
			if(!service) {
				deferred.reject('Error retriving service');
				return;
			}
			
			Templates.get(service.TemplateID).then(function(template) {
			
				socket.emit('remote/service/update', {service:service, template:template}, function(result) {
					if(result) {
						deferred.resolve(result);
					} else {
						Logger.error('[COMM | UTIL | reinstallService] Internal Error (SOCKET)');
						deferred.reject('Internal error...');
					}
				})
			}, function(err) {
			});
			
		}, function(err) {
			Logger.error('[COMM | UTIL | reinstallService - Services.Get] Internal Error');
			deferred.reject('Internal error...');
		})
		
		
		return deferred.promise;
	}
	
	socket.startService = function(serviceID) {
		var deferred = Q.defer();
		
		Services.get(serviceID).then(function(service) {
			
			if(!service) {
				deferred.reject('Error retriving service');
				return;
			}
			
			Templates.get(service.TemplateID).then(function(template) {
			
				socket.emit('remote/service/start', {service:service, template:template}, function(result) {
					if(result) {
						deferred.resolve(result);
					} else {
						Logger.error('[COMM | UTIL | startService] Internal Error (SOCKET)');
						deferred.reject('Internal error...');
					}
				})
			}, function(err) {
			});
			
		}, function(err) {
			Logger.error('[COMM | UTIL | startService - Services.Get] Internal Error');
			deferred.reject('Internal error...');
		})
		
		
		return deferred.promise;
	}
	
	socket.stopService = function(serviceId) {
		var deferred = Q.defer();
		
		Services.get(serviceID).then(function(service) {
			
			if(!service) {
				deferred.reject('Error retriving service');
				return;
			}
			
			Templates.get(service.TemplateID).then(function(template) {
			
				socket.emit('remote/service/stop', {service:service, template:template}, function(result) {
					if(result) {
						deferred.resolve(result);
					} else {
						Logger.error('[COMM | UTIL | stopService] Internal Error (SOCKET)');
						deferred.reject('Internal error...');
					}
				})
			}, function(err) {
			});
			
		}, function(err) {
			Logger.error('[COMM | UTIL | stopService - Services.Get] Internal Error');
			deferred.reject('Internal error...');
		})
		
		
		return deferred.promise;
	}
}